<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/route/list', 'ApiController@routeList');

Route::get('/station/list/{routeId}', 'ApiController@stationList');

Route::post('/point/mark/create', 'ApiController@markPoint');

Route::get('/point/mark/list', 'ApiController@markPointList');

Route::get('/station/list', 'ApiController@stationsSimpleList');
