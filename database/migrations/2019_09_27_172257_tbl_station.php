<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblStation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('station', function(Blueprint $table){
            $table->bigIncrements("id");
            $table->char("name", 128);
            $table->bigInteger('route_id')->unsigned();
            $table->integer('sort_order')->default(0);
            $table->foreign('route_id')->references('id')->on('route')->onDelete('cascade');
            $table->decimal('latitude', 12, 10);
            $table->decimal('longitude', 12, 10);
        });

//        Schema::create('route_station', function(Blueprint $table){
//            $table->bigIncrements("id");
//            $table->bigInteger('route_id')->unsigned();
//            $table->bigInteger('station_id')->unsigned();
//
//            $table->foreign('route_id')->references('id')->on('route')->onDelete('cascade');
//            $table->foreign('station_id')->references('id')->on('station')->onDelete('cascade');
//        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
//        Schema::dropIfExists('route_station');
        Schema::dropIfExists('station');
    }
}
