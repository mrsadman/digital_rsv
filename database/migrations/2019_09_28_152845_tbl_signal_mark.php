<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblSignalMark extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('signal_mark', function(Blueprint $table){
            $table->bigIncrements('id');
            $table->char('signal_strength', 128)->default('0');
            $table->char('signal_type', 128);
            $table->char('mcc_operator', 128);
            $table->char('device_type', 128);
            $table->char('device_model', 128);
            $table->char('sys_version', 128);
            $table->char('client_ver', 128);
            $table->decimal('latitude', 13,10);
            $table->char('longitude', 13,10);
            $table->dateTime('timestamp');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('signal_mark');
    }
}
