import json


data = ""
with open('route.geojson', 'r') as f:
	data = json.loads(f.read())

counter = 0

for dataItem in data["features"]:
	counter +=1
	print(
		'["route_id" => 1,"name" => "{}","sort_order" => {},"latitude" => "{}","longitude" => "{}"],' \
		.format(dataItem["properties"]["description"].replace('</br>', ''), counter, dataItem["geometry"]["coordinates"][0], dataItem["geometry"]["coordinates"][1])
		)
