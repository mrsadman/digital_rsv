<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Support\Responsable;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\Route;
use App\Models\Station;
use App\Models\SignalMark;
use App\Http\Requests\SignalMarkRequest;

class ApiController extends Controller{

    public function routeList(){
        $items = Route::all();

        $root = array();
        $root["type"] = 'FeatureCollection';

        foreach ($items as $item) {
            $root["features"][] = array(
                "type" => "Feature",
                "geometry" => [
                    "type"=> "LineString",
                    "coordinates" => json_decode($item->coordinates)
                ],
                "properties" => array(
                    "uid"=> $item->id,
                    "name"=> $item->name,
                )
            );
        }

        return response()->json($root, 200);
    }

    public function stationList($routeId){
        $items = Station::where(['route_id' => $routeId])->get();

        $root = array();
        $root["type"] = 'FeatureCollection';

        foreach ($items as $item) {
            $root["features"][] = array(
                "type" => "Feature",
                "geometry" => [
                    "type"=> "Point",
                    "coordinates"=> [
                        $item->latitude,
                        $item->longitude
                    ]
                ],
                "properties" => array(
                    "uid"=> $item->id,
                    "name"=> $item->name,
                )
            );
        }

        return response()->json($root, 200);
    }

    public function markPoint(SignalMarkRequest $request){
        $validatedData = $request->validated();
        $signal = new SignalMark($validatedData);
        if(!$signal->save()){
            return Response()->json(
                "Не удалось сохранить информацию",
                Response::HTTP_BAD_REQUEST
            );

        }
        return Response("OK", Response::HTTP_CREATED);
    }

    public function stationsSimpleList(){
        // $items = Station::all(['id', 'name'])->toArray();
        $items = [
            ['id' => 1, 'name' => 'Москва'],
            ['id' => 2, 'name' => 'Адлер'],
            ['id' => 3, 'name' => 'Липецк'],
            ['id' => 4, 'name' => 'Воронеж'],
            ['id' => 5, 'name' => 'Тула'],
        ];
        return Response(
            json_encode($items),
            Response::HTTP_OK
        );
    }

    public function markPointList(){
        $items = SignalMark::all();

        $root = array();
        $root["type"] = 'FeatureCollection';

        foreach ($items as $item) {
            $root["features"][] = array(
                "type" => "Feature",
                "geometry" => [
                    "type"=> "Point",
                    "coordinates" => [
                        0 => $item->longitude,
                        1 => $item->latitude
                    ]
                ],
                "properties" => array(
                    "uid"=> $item->id,
                    "signal_strength"=> $item->signal_strength,
                    "signal_type" => $item->signal_type,
                    "mcc_operator" => $item->mcc_operator
                )
            );
        }

        return response()->json($root, 200);
    }

}