<?php

namespace App\Http\Requests;

use App\Http\Requests\FormRequestEx;

class SignalMarkRequest extends FormRequestEx
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'signal_strength' => "required|string",
            'signal_type' => "required|string",
            'mcc_operator' => "required|string",
            'device_type' => "required|string",
            'device_model' => "required|string",
            'sys_version' => "required|string",
            'client_ver' => "required|string",
            'latitude' => "required|numeric",
            'longitude' => "required|numeric",
            'timestamp' => "required|string"
        ];

        // при редактировании добавляем дополнительное условие на существование поля answer_id
        // операцию редактирования определяем по типу запроса (http verb)
//        if($this->method() == 'PUT'){
//            $rules["answers.*.answer_id"] = "required|integer|distinct";
//        }
//
        return $rules;
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
//    public function messages()
//    {
        # custom field messages sample
//        return [
//            'email.required' => 'Email is required!',
//            'name.required' => 'Name is required!',
//            'password.required' => 'Password is required!'
//        ];
//    }

}
