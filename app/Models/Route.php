<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Answer
 * @package App
 *
 * класс модели данных для таблицы `answer`
 */
class Route extends Model
{
    protected $table = 'route';
    /**
     * Отключение использования timestamp полей в модели
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
//    protected $fillable = ["question_id", "answer", "sort_order", "active"];

    /**
     * Возвращает связанную по внешнему ключу модель Qestion
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
//    public function question(){
//        return $this->belongsTo("App\Question");
//    }

    /**
     * Возвращает массив связанных по внешнему ключу результатов опроса
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
//    /

}
