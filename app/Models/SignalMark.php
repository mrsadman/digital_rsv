<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Answer
 * @package App
 *
 * класс модели данных для таблицы `answer`
 */
class SignalMark extends Model
{
    protected $table = 'signal_mark';
    /**
     * Отключение использования timestamp полей в модели
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'signal_strength',
        'signal_type',
        'mcc_operator',
        'device_type',
        'device_model',
        'sys_version',
        'client_ver',
        'latitude',
        'longitude',
        'timestamp'
    ];

    /**
     * Возвращает связанную по внешнему ключу модель Qestion
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
//    public function question(){
//        return $this->belongsTo("App\Question");
//    }

    /**
     * Возвращает массив связанных по внешнему ключу результатов опроса
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
//    /

}
