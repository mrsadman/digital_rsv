<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Mobile GIS</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <script src="{{ asset('js/leaflet.js') }}"></script>
        <script src="{{ asset('js/leaflet.ajax.min.js') }}"></script>
        <script src="{{ asset('js/leaflet-heat.js') }}"></script>

        <link rel="stylesheet" type="text/css" href="{{ asset('css/leaflet.css') }}"/>

    </head>
    <body>
        <div id="app">
            <map-component></map-component>
        </div>

        <script src="{{ asset('js/app.js') }}"></script>
    </body>
</html>
